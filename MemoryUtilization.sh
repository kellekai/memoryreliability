#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <pid> <{Rss,Private,Shared,Swap,Pss>" >&2
  exit 1
fi

echo 0 $(cat /proc/$1/smaps  | grep $2 | awk '{print $2}' | sed 's#^#+#') | bc

