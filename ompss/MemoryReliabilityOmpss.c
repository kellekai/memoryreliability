/*
 * To compile
 * ~/apps/mcxx-rsl/bin/mcc --ompss -g --debug -O0 -k -o TaskRecoveryDemoSimple pmu.o TaskRecoveryDemoSimple.c
 *
 * To run
 * NX_ARGS="--faults-log-file /home/ferad/workspace/TaskRecoveryDemo/FaultLogger.log" ./TaskRecoveryDemoSimple -m 100 -s 0 -d -o /home/ferad/workspace/TaskRecoveryDemo/FaultLogger.log
 * NX_ARGS="--faults-log-file /home/ferad/workspace/TaskRecoveryDemo/FaultLogger.log" ./TaskRecoveryDemoSimple -m 100 -s 0 -o /home/ferad/workspace/TaskRecoveryDemo/FaultLogger.log
 */
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include "../lib/pmu.h"
#include "../lib/defines.h"

#pragma omp target device(smp) copy_deps
#pragma omp task inout( [numBytes] data) recover
void swapBits(unsigned char *data, size_t numBytes);

//
// The main daemon task
//
void ompss_zero_one_test(void* buffer, unsigned int num_bytes);

void log_message(char* message);
void log_local_mem_errors(int local_mem_errors); // Logs local memory errors
unsigned char parse_arguments(int argc, char* argv[]);
void print_usage();
void check_no_daemon_running();
void initialize_memory();
void start_daemon();
void stop_daemon();
void start_client();
void sigterm_signal_handler(int signum);
void sigint_signal_handler(int signum);
time_t get_formatted_timestamp(char* out_time_formatted, unsigned char num_bytes);

char PidFileName[255] = "pid.txt";
void daemon_pid_write_to_file(pid_t pid);
unsigned char daemon_pid_file_exists();
pid_t daemon_pid_read_from_file();
void daemon_pid_delete_file();

int read_temperature();
char TemperatureFileName[] = "/sys/class/thermal/thermal_zone0/temp";

unsigned long long NumBytes = 0;
char OutFile[255] = "MemoryReliability.log";
unsigned int SleepTime = 0;
char HostName[255];
void* Mem = NULL;
unsigned char ExitNow = 0;
unsigned char IsDaemonStart = 0;
unsigned char IsDaemonStop = 0;

char WarningFile[255] = "";
unsigned int WarningRate = 0;
unsigned int NumErrors = 0;

int main(int argc, char* argv[])
{
    char is_init = parse_arguments(argc, argv);
    if (!is_init)
    {
        print_usage(argv[0]);
        return EXIT_FAILURE;
    }

    if (IsDaemonStop)
    {
        stop_daemon();
    }
    else if (IsDaemonStart)
    {
        check_no_daemon_running();
        start_daemon();
    }
    else
    {
        start_client();
    }

    return EXIT_SUCCESS;
}

void swapBitsTestError(unsigned char *data, size_t numBytes)
{
    static int counter = 0;
    uintptr_t* dataPtr = (uintptr_t*)data;
    if (counter == 0)
    {
       *dataPtr = 0x8;
    }
    else
    {
       *dataPtr = ~0x0;
    }
    counter++;
}

void swapBits(unsigned char *data, size_t numBytes)
{
   static int flagTestNanosFaultDetection = 0;
   const uintptr_t ZERO_MASK = 0x0;
   const uintptr_t ONE_MASK = ~0x0;
   unsigned char* memStart = (unsigned char*)data;
   unsigned char* memEnd = memStart + numBytes;

   uintptr_t* wordInd = (uintptr_t*)memStart;
   uintptr_t testMask = ZERO_MASK;

   if (*wordInd == ZERO_MASK)
   {
      testMask = ONE_MASK;
   }

   for (wordInd = (uintptr_t*)memStart; wordInd < (uintptr_t*)memEnd; wordInd++)
   {
      *wordInd = testMask;
   }

   if (flagTestNanosFaultDetection == 0)
   {
      *memStart = 0x8;
      flagTestNanosFaultDetection = 1;
   }
}

void print_usage(char* program_name)
{
    printf("Ompss Memory scanner by Ferad Zyulkyarov\n");
    printf("This program starts a daemon for memory test or stops an already running daemon.\n");
    printf("Options: [-d] [-c] -m [-o] [-s] [-wn] [-wf]\n");
    printf("    -d: start as daemon. If not passed will start as a foreground process.\n");
    printf("    -c: stop the daemon.\n");
    printf("    -m: the size of the memory to test in MB.\n");
    printf("    -o: log file name [default=%s].\n", OutFile);
    printf("    -s: sleep time in seconds [default=%u].\n", SleepTime);
    printf("    -wn: warning rate [default=%u].\n", WarningRate);
    printf("    -wf: warning file [default=%s].\n", WarningFile);
    printf("Example to start a daemon:\n");
    printf("    %s -d -m 1024 -s 10 -o full_path_to_logfile.log -- run as a daemon, test 1024MB of memory, and sleep for 10sec, write logs to logfile.log.\n", program_name);
    printf("    %s -d -m 1024 -s 10 -o full_path_to_logfile.log -wn 15 -wf full_path_to_warningfile.txt -- run as a daemon, test 1024MB of memory, and sleep for 10sec, write logs to logfile.log, if there are more than (-wn) 10 errors detected write the name of the host to file warning.txt\n", program_name);
    printf("Example to stop a daemon: \n");
    printf("    %s -c\n", program_name);
    printf("Example to run as a foreground process:\n");
    printf("    %s -m 1024 -s 10 -- run as a daemon, test 1024MB of memory, and sleep for 10sec.\n", program_name);
}

unsigned char parse_arguments(int argc, char* argv[])
{
    int i = 1;
    unsigned int mega = 0;
    unsigned char is_success = 0;

    memset(HostName, 0, 255);
    gethostname(HostName, 254);

    for (i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-c") == 0)
        {
            IsDaemonStop = 1;
        }
        if (strcmp(argv[i], "-d") == 0)
        {
            IsDaemonStart = 1;
        }
        if (strcmp(argv[i], "-m") == 0)
        {
            i++;
            mega = (unsigned int)atoi(argv[i]);
            NumBytes = (unsigned long long)mega * (unsigned long long)MEGA;
        }
        if (strcmp(argv[i], "-o") == 0)
        {
            i++;
            strcpy(OutFile, argv[i]);
        }
        if (strcmp(argv[i], "-s") == 0)
        {
            i++;
            SleepTime = (unsigned int)atoi(argv[i]);
        }
        if (strcmp(argv[i], "-wn") == 0)
        {
            i++;
            WarningRate = (unsigned int)atoi(argv[i]);
        }
        if (strcmp(argv[i], "-wf") == 0)
        {
            i++;
            strcpy(WarningFile, argv[i]);
        }
    }

    if (!IsDaemonStop)
    {
        printf("Host name: %s\n", HostName);
        printf("Memory size: %llu bytes = %llu MB\n", NumBytes, NumBytes/MEGA);
        printf("Log file: %s\n", OutFile);
        printf("Sleep time: %u\n", SleepTime);
        printf("Warning rate: %u\n", WarningRate);
        printf("Warning file: %s\n", WarningFile);
    }

    is_success = (NumBytes != 0) || (IsDaemonStop);

    if ((WarningRate > 0 && strlen(WarningFile) == 0) || (WarningRate == 0 && strlen(WarningFile) > 0))
    {
        is_success = 0;
        printf("Error with the provided command line arguments. Make sure that both -wn and -wf arguments are provided and correct.\n");
    }
    return is_success;
}

void log_message(char* message)
{
    char time_str[255];
    time_t time = get_formatted_timestamp(time_str, 255);
    int temperature = 0;

    FILE *f = fopen(OutFile, "a+");
    if (!f)
    {
        exit(EXIT_FAILURE);
    }
    temperature = read_temperature();

    fprintf(f, "%s,%lld,%s,%s,%d\n", time_str, (long long int)time, message, HostName, temperature);
    fclose(f);
}

void ompss_zero_one_test(void* buffer, unsigned int num_bytes)
{
    ADDRVALUE expected_value = 0x0;
    ADDRVALUE* word_ind = NULL;
    unsigned int chunk_size = 1024*1024*10;
    unsigned char* start_mem = (unsigned char*)buffer;
    unsigned char* end_mem = start_mem + num_bytes;
    unsigned char* current_chunk_start = start_mem;

    //
    // Start the HW counters for the LocalMemErrors
    //
    int local_mem_errors = startLocalMemErrorsCounters();
    if (!local_mem_errors)
    {
        char msg[] = "Cannot start LocalMemError counters.";
        log_message(msg);
        //ExitNow = 1;
        if (daemon_pid_file_exists())
        {
            daemon_pid_delete_file();
        }
    }

    while (ExitNow == 0)
    {
       for (current_chunk_start = start_mem; current_chunk_start < end_mem; current_chunk_start += chunk_size)
       {
          swapBits(current_chunk_start, chunk_size);
          #pragma omp taskwait
       }



       local_mem_errors = readLocalMemErrorsCounters();
       log_local_mem_errors(local_mem_errors);

       if (SleepTime > 0 && ExitNow == 0)
       {
           sleep(SleepTime);
       }
    }

    free(buffer);

    if (local_mem_errors)
    {
       local_mem_errors = stopLocalMemErrorsCounters();
       log_local_mem_errors(local_mem_errors);
    }
}

void check_no_daemon_running()
{
    pid_t pid = daemon_pid_read_from_file();
    if (pid != 0)
    {
        fprintf(stderr, "It appears that an instance of this daemon is running because the %s pid file exists. If you are sure that there is no daemon running delete the pid file: %s.\n", PidFileName, PidFileName);
        exit(EXIT_FAILURE);
    }
}
void initialize_memory()
{
    while(!Mem && NumBytes > MEGA)
    {
        Mem = malloc(NumBytes);
        if (!Mem)
        {
            NumBytes -= MEGA*10;
        }
    }

    if (!Mem)
    {
        char msg[255];
        sprintf(msg, "ERROR_INFO,Cannot allocate %llu number of bytes.", NumBytes);
        log_message(msg);
        sleep(2);
        if (daemon_pid_file_exists())
        {
            daemon_pid_delete_file();
        }
        exit(EXIT_FAILURE);
    }
    memset(Mem, 0x0, NumBytes);
}

void start_daemon()
{
    char start_msg[255];
    //
    // Fork child process
    //
    pid_t pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "ERROR: Cannot create child process.\n");
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        //
        // Write the pid of the daemon to the pid file
        //
        daemon_pid_write_to_file(pid);

        //
        // Child process created exit parent process.
        //
        exit(EXIT_SUCCESS);
    }

    //
    // Set file permissions for files created by our child process
    // This requires sudo.
    //
    umask(0);

    //
    // Create session for our new process.
    //
    pid_t sid = setsid();
    if (sid < 0)
    {
        fprintf(stderr, "ERROR_INFO,Cannot create session for the child process.\n");
        log_message("ERROR_INFO,Cannot create session for the child process.");
        exit(EXIT_FAILURE);
    }

    //
    // Change the current working directory
    //
    if ((chdir("/")) < 0)
    {
        fprintf(stderr, "ERROR_INFO,Cannot change the working directory.\n");
        log_message("ERROR_INFO,Cannot change the working directory.");
        exit(EXIT_FAILURE);
    }

    sprintf(start_msg, "START,%llu,%u", NumBytes, SleepTime);
    log_message(start_msg);
    printf("Daemon started\n");

    //
    // Close all standard file descriptors
    //
    fclose(stdin);
    fclose(stdout);
    fclose(stderr);

    //
    // Install signal handler that responds to kill [pid] from the command line.
    //
    signal(SIGTERM, sigterm_signal_handler);

    //
    // Ignore signal when terminal session is closed. This keeps the
    // daemon alive when the user closes the terminal session.
    //
    signal(SIGHUP, SIG_IGN);

    //
    // The daemon loop.
    //
    initialize_memory();
    //simple_memory_test(Mem, NumBytes);
    ompss_zero_one_test(Mem, NumBytes);

    exit(EXIT_SUCCESS);
}

void start_client()
{
    char start_msg[255];

    //
    // Install signal handler that responds to kill [pid] from the command line.
    //
    signal(SIGTERM, sigterm_signal_handler);

    //
    // Install signal handler that responds to Ctrl+C interrupt
    //
    signal(SIGINT, sigint_signal_handler);

    printf("Client started...\n");
    sprintf(start_msg, "START,%llu,%u", NumBytes, SleepTime);
    log_message(start_msg);

    initialize_memory();
    //simple_memory_test(Mem, NumBytes);
    ompss_zero_one_test(Mem, NumBytes);
}

void sigterm_signal_handler(int signum)
{
    ExitNow = 1;
    log_message("STOP,SIGTERM");
}
void sigint_signal_handler(int signum)
{
    ExitNow = 1;
    log_message("STOP,SIGINT");
}

void stop_daemon()
{
    pid_t pid = daemon_pid_read_from_file();
    int kill_status = 0;
    if (pid == 0)
    {
        fprintf(stderr, "ERROR: It seems that there is no daemon running. Please, check that %s file exists and contains a valid pid value. If you are sure that the daemon is running, please, kill it manually.\n", PidFileName);
    }
    else
    {
        kill_status = kill(pid, SIGTERM);
        if (kill_status == 0)
        {
            printf("The daemon stopped.\n");

            //
            // Delete the daemon pid file.
            //
            daemon_pid_delete_file();
        }
        else
        {
            fprintf(stderr, "ERROR: Failed to stop the daemon with pid %d.\n", pid);
        }
    }
}

time_t get_formatted_timestamp(char* out_time_formatted, unsigned char num_bytes)
{
    time_t rawtime;
    struct tm *info;
    time(&rawtime);
    info = localtime(&rawtime);
    strftime(out_time_formatted, num_bytes, "%x - %H:%M:%S", info);
    return rawtime;
}

void daemon_pid_write_to_file(pid_t pid)
{
    FILE *f = fopen(PidFileName, "w+");
    if (!f)
    {
        fprintf(stderr, "ERROR: Cannot create a daemon pid file.\n");
        exit(EXIT_FAILURE);
    }
    fprintf(f, "%d", pid);
    fclose(f);
}

unsigned char daemon_pid_file_exists()
{
    unsigned char is_file_exists = 0;

    FILE *f = fopen(PidFileName, "w");
    if (f)
    {
        is_file_exists = 1;
        fclose(f);
    }

    return is_file_exists;
}

pid_t daemon_pid_read_from_file()
{
    pid_t pid = 0;
    int fscanStatus = 0;
    FILE *f = fopen(PidFileName, "r");
    if (f)
    {
        int pid_int = 0;
        fscanStatus = fscanf(f, "%d", &pid_int);
        if (fscanStatus <= 0)
        {
            fprintf(stderr, "ERROR: Cannot fscanf file %s for pid.\n", PidFileName);
        }
        pid = (pid_t)pid_int;
        fclose(f);
    }
    return pid;

}
void daemon_pid_delete_file()
{
    int delete_status = remove(PidFileName);
    if (delete_status != 0)
    {
        fprintf(stderr, "ERROR: Cannot delete the file %s. Probably no such file exists.\n", PidFileName);
    }
}

int read_temperature()
{
    int temperature = 0;
    int scanfStatus = 0;

    FILE *f = fopen(TemperatureFileName, "r");

    if (f)
    {
        scanfStatus = fscanf(f, "%d", &temperature);
        fclose(f);
    }

    return temperature;
}

void log_local_mem_errors(int local_mem_errors)
{
    if (local_mem_errors != 0)
    {
        if (local_mem_errors < 0)
        {
            char err_msg[255] = "ERROR_INFO,Cannot read MemLocalErrs counter.";
            log_message(err_msg);
        }

        if (local_mem_errors > 0)
        {
            char err_msg[255];
            memset(err_msg, 0, 255);
            sprintf(err_msg, "ERROR_MEM_LOCAL,%d", local_mem_errors);
            log_message(err_msg);
        }
    }
}

int main2(int argc, char* argv[])
{
    printf("OmpSs TaskRecoveryDemoSimple.\n");
    uintptr_t data = 0x0;
    size_t numBytes = sizeof(data);

    swapBits((unsigned char*)&data, numBytes);

    #pragma omp taskwait

    printf("data = %" PRIxPTR "\n", data);

    printf("Done\n");
}
