#ifndef _MEMORYRELIABILITY_DEFS_H
#define _MEMORYRELIABILITY_DEFS_H
// DEFINITIONS
typedef uintptr_t ADDRVALUE;
typedef void* ADDRESS;

const unsigned int TCC_ACT_TEMP         = 100; // TCC activation temperature
const unsigned int TEMP_FIELD_LOW_BIT   = 16; // low bit of 6 bit temp value
const unsigned int TEMP_FIELD_OFFSET    = 412; // offset in /dev/cpu/#cpu/msr file
uint64_t TEMP_FIELD_MASK                = 0x00000000003f0000; // selects 6 bit field[22:16]
const unsigned int KILO                 = 1024;
const unsigned int MEGA                 = 1048576;
const unsigned int GIGA                 = 1073741824;

// STATIC VARIABLES
char PidFileName[255] = "pid.txt";
char TemperatureFileName[] = "/sys/class/thermal/thermal_zone0/temp";
char OutFile[255] = "MemoryReliability.log";
char WarningFile[255] = "";

unsigned int WarningRate = 0;
unsigned int NumErrors = 0;

char HostName[255];

unsigned long long NumBytes = 0;
void* Mem = NULL;

unsigned int SleepTime = 0;
unsigned char ExitNow = 0;
unsigned char IsDaemonStart = 0;
unsigned char IsDaemonStop = 0;
#endif
